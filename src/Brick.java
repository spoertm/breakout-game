import java.awt.Color;
import java.awt.geom.Rectangle2D;

public class Brick {

	private Rectangle2D.Double brick;
	static Color[] color;
	private int HP;
	static  int brickW = 80, brickH = 60;
	private int xPos, yPos;
	
	public Brick(int x, int y, int HP) {
		this.brick = new Rectangle2D.Double();
		color = new Color[3];
		Brick.color[0] = Color.red;
		Brick.color[1] = Color.blue;
		Brick.color[2] = Color.white;
		this.xPos = x;
		this.yPos = y;
		this.HP = HP;
		brick.setFrame(this.xPos, this.yPos, brickW, brickH);
	}
	
	public int getHP() {
		return this.HP;
	}
	
	public void setHP(int HP) {
		this.HP = HP;
	}
	
	public int getXPos() {
		return xPos;
	}
	
	public int getYPos() {
		return yPos;
	}
	
	public void setXPos(int x) {
		this.xPos = x;
	}
	
	public void setYPos(int y) {
		this.yPos = y;
	}
	
	public Rectangle2D getShape() {
		return brick;
	}
	
	public Rectangle2D getBounds() {
		return new Rectangle2D.Double(getXPos(), getYPos(), brickW, brickH);
	}
	
}
