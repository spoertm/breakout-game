import java.awt.Color;
import java.awt.geom.Rectangle2D;

public class Platform {

	private Rectangle2D.Double platform;
	private Color color;
	private int platformH, platformW;
	private int xPos, yPos, xSpeed;

	public Platform() {
		this.platform = new Rectangle2D.Double();
		this.color = Color.WHITE;
		this.platformH = 14;
		this.platformW = 120;
		this.xPos = 390;
		this.yPos = 550;
		this.xSpeed = 50;
		
		platform.setFrame(xPos, yPos, platformW, platformH);
	}

	public Color getColor() {
		return color;
	}
	
	public Rectangle2D getShape() {
		return platform;
	}
	
	public int getXPos(){
		return xPos;
	}
	
	public int getYPos() {
		return yPos;
	}
	
	public int getPlatformW() {
		return platformW;
	}
	
	public void move(int dir) {
		if(dir == 1) {
			this.xPos -= xSpeed;
			if(this.xPos < 0) this.xPos = 0;
			platform.setFrame(this.xPos, this.yPos, platformW, platformH);
		}
		
		if(dir == 2) {
			this.xPos += xSpeed;
			if(this.xPos > 900 - platformW) this.xPos = 900 - platformW;
			platform.setFrame(this.xPos, this.yPos, platformW, platformH);
		}
	}
}
