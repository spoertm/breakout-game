import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

public class Frame extends JFrame implements ActionListener {

	Ball ball;
	ControlPanel cp;

	private Timer timer;
	static int frameX, frameY;

	public Frame(String title) {
		super(title);
		frameX = 900;
		frameY = 600;
		setSize(frameX, frameY);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setVisible(true);

		cp = new ControlPanel();
		this.add(cp);
		
		timer = new Timer(4, this);
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		cp.move();
	}
}