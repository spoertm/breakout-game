import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Ball {
    private int windowWidth = Frame.frameX;
    //private int windowHeight = Frame.frameY - 20;
    
    private Ellipse2D.Double ball;
    private Color color;
    private int radius;
    private int ySpeed, xSpeed;
    private int xPos, yPos;
    
    public Ball() {
        ball = new Ellipse2D.Double();
        color = Color.YELLOW;
        radius = 8;
        xPos = 450;
        yPos = 435;
        xSpeed = 1;
        ySpeed = -1;
    }
    
    public Color getColor() {
        return color;
    }
    
    public Ellipse2D getShape() {
        return ball;
    }
    
    public int getXPos() {
    	return xPos;
    }
    
    public int getYPos() {
    	return yPos;
    }
    
    public void setXPos(int xPos) {
    	this.xPos= xPos;
    }
    
    public void setYPos(int yPos) {
    	this.yPos = yPos;
    }
    
    public int getRadius() {
    	return radius;
    }
    
    public int getXSpeed() {
    	return xSpeed;
    }
    
    public int getYSpeed() {
    	return ySpeed;
    }
    
    public void setXSpeed(int xSpeed) {
    	this.xSpeed = xSpeed;
    }
    
    public void setYSpeed(int ySpeed) {
    	this.ySpeed = ySpeed;
    }

    public void move() {
    	if ( (xPos > windowWidth - radius && xSpeed > 0) 
          || (xPos < radius && xSpeed < 0)) {
            xSpeed = -xSpeed;
        }
        
        if (yPos < radius && ySpeed < 0) ySpeed = -ySpeed;
        
        xPos += xSpeed;
        yPos += ySpeed;
        
        ball.setFrame(xPos - radius, yPos - radius, radius * 2, radius * 2);
    }
    
    public Rectangle2D getBounds() {
		return new Rectangle2D.Double(ball.getX() - 1, ball.getY() - 1, 2 * radius + 3, 2 * radius + 3);
	}
}