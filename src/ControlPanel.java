import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ControlPanel extends JPanel implements KeyListener {

	private int LEFT, RIGHT;
	private int points, life;
	private int columns, rows;
	private Ball ball;
	private Platform platform;
	private Brick[] brick;
	private JLabel score, HP, dead;
	private Color c = new Color(1f, 0f, 0f, 0f);

	public ControlPanel() {
		this.setVisible(true);
		this.setSize(Frame.frameX, Frame.frameY);
		this.setBackground(Color.DARK_GRAY);
		this.LEFT = 1;
		this.RIGHT = 2;
		this.columns = 10;
		this.rows = 3;
		this.ball = new Ball();
		this.platform = new Platform();
		this.brick = new Brick[rows * columns];
		this.points = 0;
		this.life = 3;
		
		score = new JLabel();
		score.setFont(new Font("Comic Sans", Font.PLAIN, 30));
		score.setSize(100,50);
		this.add(score);
		
		HP = new JLabel();
		HP.setFont(new Font("Comic Sans", Font.PLAIN, 30));
		HP.setSize(100,50);
		this.add(HP);
			
		dead = new JLabel();
		dead.setFont(new Font("Comic Sans", Font.PLAIN, 30));
		dead.setText("YOU DIED");
		dead.setSize(100, 100);
		dead.setLocation(0,0);
		dead.setVisible(false);
		this.add(dead);
		
		int j = 0;
		for (int i = 0; i < rows; i++) {
			for (int k = 0; k < columns; k++) {
				brick[j] = new Brick((k * Brick.brickW) + 50, (i * Brick.brickH) + 50, i+1);
				j++;
			}
		}
		
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D betterBrush = (Graphics2D) g;

		Color savedColor = betterBrush.getColor();
		
		betterBrush.setColor(platform.getColor());
		betterBrush.fill(platform.getShape());

		int k = 0;
		for(int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if(brick[k].getHP() == 3) betterBrush.setColor(Brick.color[2]);
				else if(brick[k].getHP() == 2) betterBrush.setColor(Brick.color[1]);	
				else if(brick[k].getHP() == 1) betterBrush.setColor(Brick.color[0]);	
				else if(brick[k].getHP() == 0) betterBrush.setColor(c);	
				betterBrush.fill(brick[k].getShape());
				k++;
			}
		}
		
		betterBrush.setColor(ball.getColor());
		betterBrush.fill(ball.getShape());
		betterBrush.setColor(savedColor);
		
		score.setLocation(50, 330);
		score.setText("Score: " + points);
		
		HP.setLocation(50, 360);
		HP.setText("HP: " + life);
		
	}

	public void move() {		
		//Start paddle collision
		if (ball.getXPos() +  ball.getRadius()   >=  platform.getXPos() && 
			ball.getXPos() <= platform.getXPos() +   platform.getPlatformW() &&
		 	ball.getYPos() +  ball.getRadius()   ==  platform.getYPos()) {
			
			ball.setYSpeed(-ball.getYSpeed());
			}
		//End of paddle collision
		
		//Start of brick collision
		for(int i = 0; i < rows * columns; i++) {

			if (ball.getXPos() + ball.getRadius() >= brick[i].getXPos() && 
			    ball.getXPos() + ball.getRadius() <= brick[i].getXPos() + Brick.brickW && 
			    ball.getYPos() + ball.getRadius() == brick[i].getYPos() ||
			    ball.getXPos() + ball.getRadius() >= brick[i].getXPos() && 
				ball.getXPos() + ball.getRadius() <= brick[i].getXPos() + Brick.brickW &&
				ball.getYPos() - ball.getRadius() == brick[i].getYPos() + Brick.brickH) {
				
				ball.setYSpeed(-ball.getYSpeed());
				
				points++;
				
				if (this.ball.getBounds().intersects(brick[i].getBounds())) {
					if (brick[i].getHP() == 3) brick[i].setHP(2);
					else if (brick[i].getHP() == 2) brick[i].setHP(1);
					else if (brick[i].getHP() == 1) {
						brick[i].setHP(0);
						brick[i].setXPos(9999);
					}
				}
			}
			
			if (ball.getYPos() + ball.getRadius() >= brick[i].getYPos() && 
			    ball.getYPos() + ball.getRadius() <= brick[i].getYPos() + Brick.brickH && 
			    ball.getXPos() + ball.getRadius() == brick[i].getXPos() ||
			    ball.getYPos() + ball.getRadius() >= brick[i].getYPos() && 
			    ball.getYPos() + ball.getRadius() <= brick[i].getYPos() + Brick.brickH && 
			    ball.getXPos() - ball.getRadius() == brick[i].getXPos() + Brick.brickW) {
				
			    ball.setXSpeed(-ball.getXSpeed());
			   
			    points++;
			    
			    if (this.ball.getBounds().intersects(brick[i].getBounds())) {
					if (brick[i].getHP() == 3) brick[i].setHP(2);
					else if (brick[i].getHP() == 2) brick[i].setHP(1);
					else if (brick[i].getHP() == 1) {
						brick[i].setHP(0);
						brick[i].setXPos(9999);
					}
				}
			}
			
			if(ball.getYPos() > platform.getYPos() + 200) {
				life--;
				if(life <= 0) {
					ball.setXPos(99999);
					ball.setYPos(100);
					ball.setXSpeed(0);
					ball.setYSpeed(0);
					dead.setVisible(true);
				}
				else {
					ball.setXPos(450);
					ball.setYPos(535);
					ball.setYSpeed(-1);
					ball.setXSpeed(1);
				}
				
				}
		}
		//End of brick collision
		
			ball.move();
			this.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		if (code == KeyEvent.VK_RIGHT)
			platform.move(RIGHT);
		if (code == KeyEvent.VK_LEFT)
			platform.move(LEFT);
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
}